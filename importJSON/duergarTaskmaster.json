[
    {
    "name": "Duergar Taskmaster",
    "relativeURL": "Monsters.aspx?ID=170",
    "familyName": "Duergar",
    "level": "2",
    "alignment": "Lawful Evil",
    "creatureType": "Humanoid",
    "size": "Medium",
    "nethysUrl": "https://2e.aonprd.com/Monsters.aspx?ID=170",
    "recallKnowledgeText": "Recall Knowledge (Society): DC 16",
    "flavorText": "Duergar priests of the taskmaster god Droskar often assume leadership roles within their communities, advancing the goals of their deity through coercion and displays of force. Often referred to as duergar taskmasters, these leaders often issue commands to duergar subordinates and slaves in the same breath, treating the two almost as if they were interchangeable. As a result, duergar taskmasters are loathed by both slave and slaver alike.<br><br>",
    "rarity": "common",
    "traits": [
      {
        "traitName": "Duergar",
        "traitSummary": "Subterranean kin of the dwarves, duergar typically have darkvision and immunity to poison. They are not easily fooled by illusions.",
        "traitURL": "https://2e.aonprd.com/Traits.aspx?ID=53"
      },
      {
        "traitName": "Dwarf",
        "traitSummary": "A creature with this trait is a member of the dwarf ancestry. Dwarves are stout folk who often live underground and typically have darkvision. An ability with this trait can be used or selected only by dwarves. An item with this trait is created and used by dwarves.",
        "traitURL": "https://2e.aonprd.com/Traits.aspx?ID=54"
      },
      {
        "traitName": "Humanoid",
        "traitSummary": "Humanoid creatures reason and act much like humans. They typically stand upright and have two arms and two legs.",
        "traitURL": "https://2e.aonprd.com/Traits.aspx?ID=91"
      }
    ],
    "sourceDoc": "Bestiary pg. 139",
    "perceptionMod": "+8",
    "perceptionText": "darkvision",
    "languagesList": "Common, Dwarven, Undercommon",
    "skills": [
      {
        "name": "Athletics",
        "mod": "+7"
      },
      {
        "name": "Deception",
        "mod": "+7"
      },
      {
        "name": "Intimidation",
        "mod": "+7"
      },
      {
        "name": "Occultism",
        "mod": "+5"
      },
      {
        "name": "Religion",
        "mod": "+6"
      },
      {
        "name": "Survival",
        "mod": "+6"
      }
    ],
    "attributes": {
      "str": "+2",
      "dex": "+0",
      "con": "+2",
      "int": "+0",
      "wis": "+2",
      "cha": "+3"
    },
    "items": [
      {
        "name": "chain mail"
      },
      {
        "name": "maul"
      },
      {
        "name": "religious symbol"
      }
    ],
    "AC": "18",
    "saves": [
      {
        "name": "Fort",
        "mod": "+8"
      },
      {
        "name": "Ref",
        "mod": "+4"
      },
      {
        "name": "Will",
        "mod": "+8"
      },
      {
        "name": "all",
        "saveDetail": "+2 status to all saves vs. magic, iron mind"
      }
    ],
    "HP": "30",
    "defensiveAbilities": [
      {
        "name": "Iron Mind",
        "actionCost": "passive",
        "actionText": "Duergar taskmasters automatically disbelieve all illusions of 1st level or lower."
      },
      {
        "name": "Light Blindness",
        "actionCost": "passive",
        "actionText": "When first exposed to bright light, the monster is <a style=\"text-decoration:underline\" href=\"https://2e.aonprd.com/https://2e.aonprd.com/Conditions.aspx?ID=1\">blinded</a> until the end of its next turn. After this exposure, light doesn’t blind the monster again until after it spends 1 hour in darkness. However, as long as the monster is in an area of bright light, it’s <a style=\"text-decoration:underline\" href=\"https://2e.aonprd.com/Conditions.aspx?ID=7\">dazzled</a>."
      }
    ],
    "speed": "20 feet",
    "attackActions": [
      {
        "name": "Maul",
        "actionCost": 1,
        "modifier": "+8",
        "traits": [
          "shove"
        ],
        "damageRolls": [
          "1d12+2 bludgeoning"
        ]
      }
    ],
    "spellAbilities": [
      {
        "name": "Divine Prepared Spells",
        "DC": "18",
        "spellLevels": [
          {
            "spells": [
              {
                "spellName": "fear",
                "nethysUrl": "https://2e.aonprd.com/Spells.aspx?ID=110"
              },
              {
                "spellName": "harm",
                "nethysUrl": "https://2e.aonprd.com/Spells.aspx?ID=146"
              },
              {
                "spellName": "magic weapon",
                "nethysUrl": "https://2e.aonprd.com/Spells.aspx?ID=182"
              }
            ],
            "level": "1st"
          },
          {
            "spells": [
              {
                "spellName": "detect magic",
                "nethysUrl": "https://2e.aonprd.com/Spells.aspx?ID=66"
              },
              {
                "spellName": "shield",
                "nethysUrl": "https://2e.aonprd.com/Spells.aspx?ID=280"
              }
            ],
            "level": "Cantrips (1st)"
          }
        ]
      },
      {
        "name": "Occult Innate Spells",
        "DC": "18",
        "spellLevels": [
          {
            "spells": [
              {
                "spellName": "enlarge",
                "nethysUrl": "https://2e.aonprd.com/Spells.aspx?ID=102",
                "detailText": "(self only)"
              },
              {
                "spellName": "invisibility",
                "nethysUrl": "https://2e.aonprd.com/Spells.aspx?ID=164",
                "detailText": "(self only)"
              }
            ],
            "level": "2nd"
          }
        ]
      }
    ],
    "offensiveAbilities": [
      {
        "name": "Take Them Down!",
        "actionCost": 1,
        "actionText": "The duergar taskmaster smashes their maul into the ground and invokes Droskar’s name to rally their allies to action. All allied duergars of equal or lower <a style=\"text-decoration:underline\" href=\"https://2e.aonprd.com/Rules.aspx?ID=31\"><i>level</i></a> that are within 20 feet of the duergar taskmaster gain a +1 status bonus to attack rolls and damage rolls until the end of the duergar taskmaster’s next turn."
      }
    ],
    "imageFilename": "Duergar_DuergarTaskmaster.png"
  }
]